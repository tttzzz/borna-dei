<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'homeController@index')->name('home');
Route::get('/search', 'SearchController@search')->name('search');
Route::view('/about-us', 'pages.about')->name('about');
Route::view('/contact-us', 'pages.contact')->name('contact');
Route::get('/npr/{id}', 'SearchController@npr')->name('npr');
Route::post('/order', function () {
    $ok= true;
    return view('landing.home',compact('ok'));
})->name('order');
