<?php

namespace App\Repository;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use KubAT\PhpSimple\HtmlDomParser;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use PHPHtmlParser\Exceptions\CircularException;
use PHPHtmlParser\Exceptions\CurlException;
use PHPHtmlParser\Exceptions\NotLoadedException;
use PHPHtmlParser\Exceptions\StrictException;

trait OffroadeSearchRepository
{

    public function searchOffroadePartOffroade($part)
    {
        return $this->newSearch($part);
        $client = $this->getClientOffroade();

        $search = $this->partSearchOffroade($part, $client);
        if( $search == null) return null;
        if ($search->getStatusCode() !== 200) {
            $client = $this->offroade_loginOffroade($client);
        }
        $search = $this->partSearchOffroade($part, $client);
        if( $search == null) return null;
        return $this->response_to_arrayOffroade($search);

    }

    public function newSearch($part)
    {
        try{
            $client = $this->getClientOffroade();
            $response = $client->request('GET', '/parts-search/'.$part);
            $dom = HtmlDomParser::str_get_html($response->getBody()->getContents());
            $dom = $dom->find('part-result')[0];
            $title =strip_tags($dom->find('section > h2')[0]->outertext);
            $name = trim(explode(' - ',$title)[0]);
            $desc= trim(explode(' - ',$title)[1]);
            $weight = "";

            $peirce = "";
            foreach ($dom->find('dl > dd') as $item){
                if (is_integer(strpos(strip_tags($item->outertext), "lbs"))) {
                    $weight = str_replace("lbs", "", strip_tags($item->outertext));
                }
                if (is_integer(strpos(strip_tags($item->outertext), "$"))) {
                    $peirce = str_replace("$", "", strip_tags($item->outertext));
                }
            }
            $res = [[
                'PartNumber' => $name,
                'Description' =>$desc,
                'ListPrice' => str_replace(",",'',$peirce),
                'finalPrice' => 'unavailable',
                'UnitWeight' => $weight,
            ]];

        }catch (\Exception $exception){
            $res = [];
//            dd($exception);
        }
//        dd($res);
        return  $res ;
    }


    /**
     * @return Client
     */
    private function getClientOffroade(): Client
    {
        $client = new Client(array(
            'headers' => config('offroade.headers'),
            'base_uri' => config('offroade.base_url'),
            'verify' => false,
        ));
        return $client;
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $search
     * @return mixed
     */
    private function response_to_arrayOffroade(\Psr\Http\Message\ResponseInterface $search)
    {
        return json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $search->getBody()->getContents()), true);
    }

    /**
     * @param Request $request
     * @param Client $client
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function partSearchOffroade($part, Client $client)
    {
        $start = microtime(true);
        try{
            $search = $client->post(config('offroade.search_url'), [
                'json' => [
                    'cartid' => 0,
                    'nc' => false,
                    'od_id' => 0,
                    'part' => $part,
                    'sort' => ''
                ]
            ]);

            \Log::info(microtime(true)-$start);
             return $search;
        }catch (Exception $exception){
            return null;
        }
    }

    /**
     * @param $client Client
     * @return Client
     * @throws ChildNotFoundException
     * @throws CircularException
     * @throws CurlException
     * @throws NotLoadedException
     * @throws StrictException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function offroade_loginOffroade($client): Client
    {

        $response = $client->request('GET', '/en/log-in');

        $dom = new Dom();
        $dom->load($response->getBody()->getContents());
        $csrf_name = $dom->find('#__VIEWSTATE')[0]->getAttribute('name');
        $csrf_value = $dom->find('#__VIEWSTATE')[0]->getAttribute('value');
        $user_name = $dom->find('#ctl05_ct0_ctl01_username')[0]->getAttribute('name');
        $password = $dom->find('#ctl05_ct0_ctl01_password')[0]->getAttribute('name');
        $tos = $dom->find('#ctl05_ct0_ctl01_tos')[0]->getAttribute('name');
        $response = $client->request('post', '/en/log-in', [
            'form_params' => [
                $csrf_name => $csrf_value,
                $user_name => config('offroade.user.username', 'borna'),
                $password => config('offroade.user.password'),
                $tos => 'on',
            ]]);

        Cache::put("offroade_cookie", $client->getConfig('cookies'), 1440);
        return $client;
//        }
    }
}
