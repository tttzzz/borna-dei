<?php

namespace App\Repository;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use PHPHtmlParser\Dom;
use Psr\Http\Message\ResponseInterface;
use KubAT\PhpSimple\HtmlDomParser;

/**
 * Trait NprSearchRepository
 * @package App\Repository
 *
 */
trait NprSearchRepository
{
    /**
     * @var $client Client
     */
    private $client;

    public function __construct()
    {
        $this->client = $this->NprGetClient();
    }

    public function searchNprPart($part)
    {
        $plain_text = $this->NprPartSearch($part);
        if (!is_int(strpos($plain_text,'table'))){ // need to login
            $this->NprSis_login();
            $plain_text = $this->NprPartSearch($part);
        }
        return $this->getResult(strtolower($plain_text));

    }

    /**
     * @return Client
     */
    private function NprGetClient(): Client
    {
        $jar = Cache::get("sis_cookie", true);
        $client = new Client(array(
            'cookies' => $jar,
            'base_uri' => config('Npr.base_url'),
            'verify' => false,
            'allow_redirects' => true,
        ));
        return $client;

    }

    /**
     * @param $part
     * @return ResponseInterface
     */
    private function NprPartSearch(string $part): string
    {
        $search = $this->client->post(
            '/sisweb/servlet/cat.cis.sis.PController.CSSISNprInfoServlet', [
            'form_params' => [
                'calledpage' => '/sisweb/sisweb/nprinfo/nprhome.jsp',
                'pnum' => $part,
                'ccode' => '',
                'ech' => '',
                'operation' => 'nprsearch'
            ],
            'headers' => config('Npr.headers.search')
        ]);
        $result = $search->getBody()->getContents();
        $result = strtolower($result);
        $result = trim($result);
        return $result;
    }

    private function NprSis_login()
    {
        $this->NprStep1();
        $this->NprStep2();
        $this->NprStep3();
        $this->NprStep4();
        $this->NprStep5();
        $this->NprStep6();
        $this->NprStep7();
        $this->NprStep8();
        Cache::put("sis_cookie", $this->client->getConfig('cookies'), 500);
    }

    private function NprStep1()
    {
        $this->client->request('GET', '/sisweb/servlet/cat.cis.sis.PController.CSSISMainServlet');
    }

    /**
     * @param Client $client
     * @return Client
     */
    private function NprStep2()
    {
        $parameters = array(
            'select_language' => 'en_US',
            'langdescription' => 'Indonesian;Chinese+(China);English;French;German;Italian;Portuguese+(Brazil);Spanish+(Latin+American);',
            'locales' => 'ba_BA;zh_CN;en_US;fr_FR;ge_GR;it_IT;po_PO;sp_SP;',
            'j_username' => config('Npr.user.username'),
            'j_password' => config('Npr.user.password'),
            'btn_login' => 'Login'
        );
        $this->client->post('/sisweb/servlet/j_security_check', [
            'form_params' => $parameters,
            'headers' => config('Npr.headers.step2')
        ]);
    }

    private function NprStep3()
    {
        $this->client->request('GET', '/sisweb/servlet/cat.cis.sis.PController.CSSISMainServlet?sourceoperation=forward_request');
    }

    private function NprStep4()
    {
        $parameters = array(
            'page' => 'SplashScreen',
            'BrowseDetails' => 'Netscape 5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
            'hostname' => '200gb' // Todo: change it for every host
        );
        $this->client->post('/sisweb/servlet/cat.cis.sis.PController.CSSISMainServlet', [
            'form_params' => $parameters,
        ]);
    }

    private function NprStep5()
    {
        $parameters = [
            'operation' => 'Agreement',
            'software_agree' => 'agree',
            'affiliation' => 'Transportation'
        ];
        $this->client->post('/sisweb/servlet/cat.cis.sis.PController.CSSISMainServlet', [
            'form_params' => $parameters,
            'headers' => config('Npr.headers.step2')
        ]);
    }

    private function NprStep6()
    {
        $this->client->request('GET', '/sisweb/sisweb/commonsearch/header_frame.jsp?mode=MainMenu&smno=&snp=null&page=null&proddesc=&calledpage=/sisweb/sisweb/homepage/homepage.jsp');
    }

    private function NprStep7()
    {
        $this->client->request('GET', '/sisweb/servlet/cat.cis.sis.PController.SISMainServletHelperServlet?calledpage=/sisweb/sisweb/homepage/homepage.jsp&snp=&mode=MainMenu&smno=&proddesc=&prefixselected=null&calledpage=/sisweb/sisweb/homepage/homepage.jsp&page=null');
    }

    private function NprStep8()
    {
        $this->client->request('GET', '/sisweb/servlet/cat.cis.sis.PController.CSSISNprInfoServlet?search=NPR&page=/sisweb/nprinfo/nprhome.jsp&nsnpval=&arrangement=&method=npr&from=mainpage');
    }

    private function getResult($text)
    {
        $dom = HtmlDomParser::str_get_html( $text );
        $dom = $dom->find('table')[1] ?? '<h2>Not found!</h2>';
        return $dom;
    }
}
