<?php

namespace App\Http\Controllers;

use App\Part;
use App\Repository\NprSearchRepository;
use App\Repository\OffroadeSearchRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use OffroadeSearchRepository, NprSearchRepository;

    public function search(Request $request)
    {

        $name = $request->search;
        $name = $this->normalize($name);
        if (!$name) {
            return back();
        }
        $part = Part::where("name", $name)->get()->first();
        if (!is_null($part)) {
            $result = $part->data;
        } else {
            $result = $this->searchOffroadePartOffroade($name);

            if (!isset($result[0]['ListPrice'])) {
                $result = [[
                    'PartNumber' => 'unavailable',
                    'Description' => 'unavailable',
                    'ListPrice' => 'unavailable',
                    'finalPrice' => 'unavailable',
                    'UnitWeight' => 'unavailable',
                ]];
                return view('search.result', compact('result'));
            }
            $new = new Part();
            $new->name = $name;
            $new->data = $result;

            $new->save();
        }

        $this->priceCalculator($result);
        return view('search.result', compact('result'));
    }

    public function npr(Request $request, $id)
    {
        $id = $this->normalize($id);
        $res = $this->searchNprPart($id);

        return view('npr', compact('res'));
    }

    private function priceCalculator(&$result)
    {
        $price = $result[0]['ListPrice'] ?? 0;
        if ($price <= 0)
            return;
        $total = $result[0]['ListPrice'];
        $result[0]['catPrice'] = $result[0]['ListPrice'];
        $result[0]['finalPrice'] = $total + ($total * 0.38);
    }

    private function normalize($part)
    {
        $nums = [
            '۰' => "0",
            '۱' => "1",
            '۲' => "2",
            '۳' => "3",
            '۴' => "4",
            '۵' => "5",
            '۶' => "6",
            '۷' => "7",
            '۸' => "8",
            '۹' => "9",

        ];
        foreach ($nums as $key => $num) {

            $part = str_replace($key, $num, $part);
        }
        return $part;
    }
}
