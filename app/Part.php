<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property array data
 * @property string name
 */
class Part extends Model
{
    protected $casts = [
        'data' => 'array'
    ];
}
