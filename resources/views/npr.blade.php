@extends('master')

@section("body")
    @include("partials.header")
   <div id="npr-result" class="ltr">
       {!! $res !!}
   </div>
    @include("partials.footer")
@endsection
