<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>برنادیزل</title>
    <link rel="shortcut icon" href="/public/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/public/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/public/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/public/fonts/vazir/style.css?v1.0.2">
{{--    <link rel="stylesheet" type="text/css" href="/public/vendor/bootstrap/css/bootstrap.css">--}}
    <link rel="stylesheet" href="/public/plugin/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/public/assets/styles/landing.css?v=1.0.1">
    <link rel="stylesheet" type="text/css" href="/public/assets/styles/media.css?v=1.0.0">
</head>
<body>
@yield('body')

{{--<script src="/public/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="/public/vendor/bootstrap/js/popper.min.js"></script>
<script src="/public/vendor/bootstrap/js/bootstrap.min.js"></script>--}}
<script src="/public/plugin/jquery/jquery-3.2.1.slim.min.js" ></script>
<script src="/public/plugin/jquery/popper.min.js"></script>
<script src="/public/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="/public/assets/script/main.js?v1.0.2"></script>
</body>
</html>
