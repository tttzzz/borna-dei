<div id="about-us">
    <div id="left-about">
    </div>
    <div id="right-about">
        <h1>برنا‌دیزل</h1>
        <h3>تامین قطعات کاترپیلار در سراسر ایران</h3>
        <p id="about-text">
            برنا دیزل از سال 1388 فعالیت خود را به پشتوانه سرمایه علمی و عملی خود در امر تامین قطعات و تعمیر دیزل ژنراتورهای کاترپیلار، غالباً در تعامل با شرکتهای حفاری نفتی، گازی و بسیاری از نیروگاههای گازی مقیاس کوچک آغاز کرد و این خدمت ملی همچنان ادامه دارد، با این امید که چرخ صنعت نفتی، گازی، راهسازی و معادن، هیچگاه از چرخش باز نایستد.
        </p>
        <div id="sig-social">
            <div class="sig">
                <div class="sigs">
                    <div id="sig-img"><img src="/public/images/sign.png" alt="برنا دیزل"></div>
                    <div class="social">
                        <div class="co-info">
                            <p id="co-name">امیر برناستی</p>
                            <p id="co-post">موسس</p>

                        </div>
                        <div>
                            <a target="_blank" href="https://www.linkedin.com/in/borna-diesel-5aa61152"><i class="fa fa-linkedin"></i></a>
                        </div>
{{--                        <div>
                            <a href="/"><i class="fa fa-twitter"></i></a>
                            <a href="/"><i class="fa fa-facebook"></i></a>
                        </div>
                        <div>
                            <a href="/"><i class="fa fa-telegram"></i></a>
                            <a href="/"><i class="fa fa-instagram"></i></a>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
