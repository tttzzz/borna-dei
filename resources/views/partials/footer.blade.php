<footer id="footer">
    <div class="abs0 footer-black-bg"></div>
    <div id="footer-content">
        <div class="footer-parts">
            <div class="header">
                درباره برنا دیزل
            </div>
            <div class="body">
                <p>برنا دیزل از سال 1388 فعالیت خود را به پشتوانه سرمایه علمی و عملی خود در امر تامین قطعات و تعمیر دیزل ژنراتورهای کاترپیلار، غالباً در تعامل با شرکتهای حفاری نفتی، گازی و بسیاری از نیروگاههای گازی مقیاس کوچک آغاز کرد و این خدمت ملی همچنان ادامه دارد، با این امید که چرخ صنعت نفتی، گازی، راهسازی و معادن، هیچگاه از چرخش باز نایستد.</p>
            </div>
        </div>
        {{--<div class="footer-parts last-parts">
            <div class="header">
                اخرین قطعات
            </div>
            <div class="body">
                <ul class="with-image">
                    <li>
                        <div class="footer-list-image"><img
                                src="http://kodesolution.website/html/j/industrypress/v2.0/demo/images/blog/ln1.jpg"
                                alt=""></div>
                        <div class="desc">
                            <h3 class="footer-list-title">فیلتر</h3>
                            <p class="footer-list-abs">فیلتر موتورهای k432</p>
                        </div>
                    </li>
                    <li>
                        <div class="footer-list-image"><img
                                src="http://kodesolution.website/html/j/industrypress/v2.0/demo/images/blog/ln1.jpg"
                                alt=""></div>
                        <div class="desc">
                            <h3 class="footer-list-title">فیلتر</h3>
                            <p class="footer-list-abs">فیلتر موتورهای k432</p>
                        </div>
                    </li>
                    <li>
                        <div class="footer-list-image"><img
                                src="http://kodesolution.website/html/j/industrypress/v2.0/demo/images/blog/ln1.jpg"
                                alt=""></div>
                        <div class="desc">
                            <h3 class="footer-list-title">فیلتر</h3>
                            <p class="footer-list-abs">فیلتر موتورهای k432</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>--}}
        {{--<div class="footer-parts more">
            <div class="header">
                بیشتر
            </div>
            <div class="body">
                <ul class="links">
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">درباره‌ما</a></li>
                    <li><a href="#">محصولات</a></li>
                    <li><a href="#">جستجو</a></li>
                </ul>
            </div>
        </div>--}}
        <div class="footer-parts">
            <div class="header">
                تماس با ما
            </div>
            <div class="body">
                <ul class="contact">
                    <li>
                        <i class="fa fa-envelope"></i>
                        <a href="mailto://info@bornadiesel.us">info@bornadiesel.us</a>
                    </li>

                    <li>
                        <i class="fa fa-phone ltr"></i>
                        <a class="ltr" href="tel://+982166608426">021 6660 8426</a>
                    </li>
                    <li>
                        <i class="fa fa-fax ltr"></i>
                        <a class="ltr" href="tel://+982166618634">021 6661 8634</a>
                    </li>
                    <li>
                        <i class="fa fa-map-marker ltr"></i>
                        <a class="ltr" href="#">سه راه آذری ۴۵ متری زرند،پلاک۱۱۹</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="cp">کلیه حقوق مادی و معنوی این سایت متعلق به <span class="yellow"><a href="/">برنادیزل</a></span> می باشد.
</div>

