<header>


    <nav id="small-nav" class=" navbar navbar-expand-lg navbar-light">
        <div class="flex">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div id="right-nav">
                <a href="tel://+982166608426">021 6660 8426</a><i class="fa fa-phone"></i>
            </div>
            <a class="navbar-brand" href="#"><img class="logo" src="/public/images/bd-s.jpg"
                                                  alt="borna-diesel"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">

            <ul>
                <li><a href="/">صفحه اصلی</a></li>
                <li><a href="{{route('contact')}}">تماس با ما</a></li>
                <li><a href="{{route('about')}}#about-us">در باره ما</a></li>
            </ul>
        </div>
    </nav>
    <nav class="abs0" id="large-nav">
        <div id="right-nav">
            <a href="tel://+982166608426">021 6660 8426</a><i class="fa fa-phone"></i>
        </div>
        <div id="center-nav">
            <ul>
                <li><a href="/">صفحه اصلی</a></li>
                <li><a href="{{route('contact')}}">تماس با ما</a></li>
                <li><a href="{{route('about')}}#about-us">در باره ما</a></li>
            </ul>
        </div>
        <div id="left-nav">
            <a href="/"><img class="logo" src="/public/images/bd.png" alt="borna-diesel"></a>

        </div>

    </nav>

    <div class="black-bg abs0">
        <div></div>
        <div>
            <div class="ltr" id="header-slogon">TODAY’S WORK, TOMORROW’S WORLD.</div>
        </div>
        {{--        @include("landing.partials.contact-info")--}}
        <div id="search-box">
            {{--            <button class="header-button"><a href="#">محصولات</a></button>--}}
            <form id="header-search" action="{{route('search')}}">
                <input type="text" placeholder="شماره قطعه" name="search" autofocus>
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            @if(isset($result))
                <div id="search-box-result">
                    <p class="desc ltr search-res"><strong>PartNumber: </strong>{{$result[0]['PartNumber'] ?? ''}}</p>
                    <p class="desc ltr search-res"><strong>Description: </strong>{{$result[0]['Description'] ?? ''}}</p>
                    <p class="desc ltr search-res"><strong>Cat Price: </strong>{{$result[0]['catPrice'] ?? ''}}
                        <span>$</span></p>
                    <p class="desc ltr search-res"><strong>Order Price: </strong>{{--{{$result[0]['finalPrice'] ?? ''}} <span>$</span>--}}</p>
                    <p class="desc ltr search-res"><strong>UnitWeight(LBS): </strong>{{$result[0]['UnitWeight'] ?? ''}}
                    </p>
                    {{--                <p class="desc ltr search-res"><strong>Name: </strong>{!!  $result['pnr_name'] ?? ''!!}</p>--}}
                    {{--                <p class="desc ltr search-res"><strong>Service Part No: </strong>{!! $result['pnr_number'] ?? '' !!}</p>--}}
                    {{--                <div class="desc ltr search-res" id="where-use"><strong>Where Use: </strong>{!! $result['pnr_desc'] ?? '' !!}</div>--}}
                    <div class="buttons">
                        <button class="btn borna-btn " id="order-btn" data-toggle="modal" data-target="#orderModal">
                            order
                        </button>
                        <a class="btn borna-btn " id="details-btn" href="{{route("npr",request('search'))}}#npr-result">Details</a>
                    </div>
                </div>
            @endif
{{--            <p class="desc"><strong>زمان تحویل:</strong>دو هفته بعد از سفارش</p>--}}
        </div>
        <div class="hidden-sm"></div>
        <div class="hidden-sm"></div>
        <div></div>

    </div>
{{--    <video muted id="headerVideo" autoplay  class="abs0" loop poster="/public/images/poster.jpg">--}}
{{--        --}}{{--        <source src="https://hw15.cdn.asset.aparat.com/aparat-video/0f73229e0f49a8d9d3a8a5dd83bc4ade23025790-360p.mp4" type="video/mp4">--}}
{{--        <source src="http://www.bornadiesel.com/public/images/lg-header.mp4" type="video/mp4">--}}
{{--    </video>--}}
</header>
<div class="modal fade ltr" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel"
     aria-hidden="true" style="display: none">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="orderModalLabel">Enter you contact info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('order')}}" method="post">
                    @csrf
                    <div class="form-group">

                        <label for="orderPhone">Phone Number</label>
                        <input type="text" class="form-control" id="orderPhone" aria-describedby="emailHelp"
                               name="phone" placeholder="0912*******">
                        <small id="emailHelp" class="form-text text-muted">We'll contact you soon.</small>
                    </div>
                    <div class="form-group">
                        <label for="name">Your name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Ali alavi">
                    </div>
                    <input type="hidden" value="{{$result[0]['PartNumber'] ?? ''}}" name="part">
                    <button type="submit" class="btn borna-btn">Send</button>
                </form>
            </div>
        </div>
    </div>
</div>
@if(isset($ok))
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="mr-auto">Message</strong>
            <small class="text-muted">11 mins ago</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">Order successfully.</div>
        <div class="toast-body">We'll call you very soon.</div>
    </div>
@endif
