<div id="product-wrapper">
    <h2>پرفروشهای برنا دیزل</h2>
    <div id="products-members-wrapper">

        <div class="product-member flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front">
                    <img src="/public/images/gear.jpeg" alt="caterpillar engine">
                </div>
                <div class="flip-card-back">
                    <a href="/">

                        <h3>مشخصات فنی</h3>
                        <p><span class="key">نام: </span> <span class="value">دنده</span></p>
                        <p><span class="key">شماره: </span> <span class="value">34K5JK2</span></p>
                        <p><span class="key">تاریخ: </span> <span class="value">20018/09/21</span></p>
                        <p><span class="key">وزن: </span> <span class="value">3KG</span></p>
                    </a>
                </div>
            </div>
        </div>


        <div class="product-member flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front">
                    <img src="/public/images/shovel.jpg" alt="caterpillar engine">
                </div>
                <div class="flip-card-back">
                    <a href="/">

                        <h3>مشخصات فنی</h3>
                        <p><span class="key">نام: </span> <span class="value">دنده</span></p>
                        <p><span class="key">شماره: </span> <span class="value">34K5JK2</span></p>
                        <p><span class="key">تاریخ: </span> <span class="value">20018/09/21</span></p>
                        <p><span class="key">وزن: </span> <span class="value">3KG</span></p>
                    </a>
                </div>
            </div>
        </div>


        <div class="product-member flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front">
                    <img src="/public/images/engine.jpg" alt="caterpillar engine">
                </div>
                <div class="flip-card-back">
                    <a href="/">

                        <h3>مشخصات فنی</h3>
                        <p><span class="key">نام: </span> <span class="value">دنده</span></p>
                        <p><span class="key">شماره: </span> <span class="value">34K5JK2</span></p>
                        <p><span class="key">تاریخ: </span> <span class="value">20018/09/21</span></p>
                        <p><span class="key">وزن: </span> <span class="value">3KG</span></p>
                    </a>
                </div>
            </div>
        </div>


        <div class="product-member flip-card">
            <div class="flip-card-inner">
                <div class="flip-card-front">
                    <img src="/public/images/C8.jpeg" alt="caterpillar engine">
                </div>
                <div class="flip-card-back">
                    <a href="/">

                        <h3>مشخصات فنی</h3>
                        <p><span class="key">نام: </span> <span class="value">دنده</span></p>
                        <p><span class="key">شماره: </span> <span class="value">34K5JK2</span></p>
                        <p><span class="key">تاریخ: </span> <span class="value">20018/09/21</span></p>
                        <p><span class="key">وزن: </span> <span class="value">3KG</span></p>
                    </a>
                </div>
            </div>
        </div>


    </div>
</div>
