<div class="container" id="part-search-result">
    <div class="row">
        <table class="table table-striped search-result">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>SearchId</th>
                <th>PartNumberDisplay</th>
                <th>Description</th>
                <th>ListPrice$</th>
                <th>UnitWeight</th>
                <th>WarehouseId</th>
                <th>Notes</th>
            </tr>
            </thead>
            <tbody>
            @foreach( $result as $value )
                <tr>
                    <td>{{$loop->index}}</td>
                    <td>{{ $value['SearchId'] }}</td>
                    <td>{{ $value['PartNumberDisplay'] }}</td>
                    <td>{{ $value['Description'] }}</td>
                    <td>{{ $value['ListPrice'] }}$</td>
                    <td>{{ $value['UnitWeight'] }}</td>
                    <td>{{ $value['WarehouseId'] }}</td>
                    <td>{{ $value['Notes'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>
